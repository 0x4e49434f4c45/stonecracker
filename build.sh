#!/bin/bash

pushd `dirname "${BASH_SOURCE[0]}"` > /dev/null
mkdir -p build

# Zip contents of source directory to datapack file
# Working directory must be set to src, otherwise paths within the zip will contain src
pushd src > /dev/null
zip -r ../build/stonecracker.zip *
popd > /dev/null

popd > /dev/null
