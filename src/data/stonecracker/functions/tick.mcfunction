# Find all falling blocks of type anvil that haven't found a block under them yet
execute as @e[type=falling_block,nbt={BlockState:{Name:"minecraft:anvil"}},tag=!HasCracked] run function stonecracker:try_crack_stone
